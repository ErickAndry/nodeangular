const express = require('express');

const router = express.Router();

const userCtrl =  require('../controllers/user')

/**
 * exo2
 */
router.get('/users/import', userCtrl.getAllUser);

/**
 * exo3 + exercice 07 
 */
router.get('/users', userCtrl.getUsers);

/**
 * exo4
 */
router.get('/users/:uuid', userCtrl.getUserById);
/**
 * exo08
 */
 router.get('/users.json', userCtrl.getAllUserJson);
/**
 * exo08
 */
 router.get('/users.xml', userCtrl.getAllUserXml);


module.exports = router;
