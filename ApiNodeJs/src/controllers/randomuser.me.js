const axios = require("axios");

let listUsers = [];

/**
 * initialisation listUsers du randomuser.me
 */
async function ImportUsers() {
  if (listUsers.length == 0) {
    const params = configGetRandom(null);
    listUsers = await getUsers(params);
  }
  return listUsers;
}
/**
 * 
 * appel service randomuser.me
 */
async function getUsers(parms) {
  let users = null;
  try {
    await axios
      .get(
        `https://randomuser.me/api/1.2/?seed=${parms.seed}&nat=${parms.pays}&results=${parms.count}`
      )
      .then((response) => {
        users = response.data;
      });
    return users;
  } catch (error) {
    console.log("error");
    return null;
  }
}

/**
 * parammettre randomuser.me
 */
function configGetRandom(data) {
  const params = {
    seed: data != null ? req.query.seed : process.env.seed,
    pays: data != null ? req.query.pays : process.env.pays,
    count: data != null ? req.query.count : process.env.count,
  };
  return params;
}

module.exports = {
  getUsers,
  configGetRandom,
  ImportUsers,
};
