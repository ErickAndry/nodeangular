const randomuser = require("./randomuser.me");
const { passwordStrength } = require("check-password-strength");
require("dotenv").config();

/**
 *
 * lister tous utilisateur exercice 02
 */
const getAllUser = async (req, res, next) => {
  try {
    const param = {
      seed: req.query.seed ? req.query.seed : process.env.seed,
      pays: req.query.pays ? req.query.pays : process.env.pays,
      count: req.query.count ? req.query.count : process.env.count,
    };
    const listRandomUser = await randomuser.getUsers(param);
    res.status(200).json({
      listRandomUser,
    });
  } catch (error) {
    res.status(500).send(error);
    next();
  }
};

/**
 * exercice 03 + exercice 07
 */
const getUsers = async (req, res, next) => {
  try {
    let users = [];
    let usersFinale = [];
    const listRandomUser = await randomuser.ImportUsers();
    await listRandomUser.results.map((userRes) => {
      const user = {
        name: userRes.name,
        email: userRes.email,
        login: {
          uuid: userRes.login.uuid,
          username: userRes.login.username,
          passwordstrength: checkPassword(userRes.login.password),//exo6
        },
        registered: userRes.registered,
        picture: {
          thumbnail: userRes.picture.thumbnail,
        },
      };
      usersFinale.push(user);
    });
    // filtre exo 7
    if (req.query) {
      users = await filttreUsers(usersFinale, req.query);
    } else {
      users = usersFinale;
    }

    res.status(200).json({
      users,
    });
  } catch (error) {
    res.status(500).send(error);
    next();
  }
};

/**
 *
 *exercice 04
 */
const getUserById = async (req, res, next) => {
  try {
    const listRandomUser = await randomuser.ImportUsers();
    let user = await listRandomUser.results.find((res) => {
      return res.login.uuid === req.params.uuid;
    });
    //exo6
    user.login["passwordstrength"] = checkPassword(user.login.password);
    res.status(200).json({
      user,
    });
  } catch (error) {
    res.status(500).send(error);
    next();
  }
};

/**
 * exercice 6
 */
function checkPassword(mdp) {
  return passwordStrength(mdp).value;
}

/**
 * exercice 07
 */
function filttreUsers(list, filter) {
  let users = [];
  users = list.filter(function (item) {
    let valueCompar = null;
    for (var key in filter) {
      let keys = key.split(".");
      if (keys.length > 1) {
        const item1 = keys[0];
        let itemd2 = keys[1];
        const itemP = item[item1];
        const itemC = itemP[itemd2];
        valueCompar = itemC;
      } else {
        valueCompar = item[key];
      }
      if (valueCompar === undefined || valueCompar != filter[key]) return false;
    }
    return true;
  });
  return users;
}

/**
 * exo 8
 */
const getAllUserJson = async (req, res, next) => {
  try {
    let users = await randomuser.ImportUsers();
    if (req.query) {
      users = await filttreUsers(users.results, req.query);
    }
    res.status(200).json({
      users,
    });
  } catch (error) {
    res.status(500).send(error);
    next();
  }
};

const getAllUserXml = async (req, res, next) => {
  try {
    let users = await randomuser.ImportUsers();
    const list =  objectToXml(users);    
    res.status(200).send(list);
  } catch (error) {
    res.status(500).send(error);
    next();
  }
};

/**
 * convert to xml
 */
function objectToXml(obj) {
  let xml = '';
  for (let prop in obj) {
    debugger;
    if (!obj.hasOwnProperty(prop)) {
      continue;
    }
    if (obj[prop] == undefined)
      continue;
    xml += "<" + prop + ">";
    if (typeof obj[prop] == "object") {
      if (obj[prop].constructor === Array) {
        for (let i = 0; i < obj[prop].length; i++) {
          xml += '<item>';
          xml += objectToXml(new Object(obj[prop][i]));
          xml += "</item>";
        }
      } else {
        xml += objectToXml(new Object(obj[prop]));
      }
    } else {
      xml += obj[prop];
    }
    xml += "</" + prop + ">";
  }
  return xml;
}


module.exports = {
  getAllUser,
  getUsers,
  getUserById,
  getAllUserJson,
  getAllUserXml,
};
