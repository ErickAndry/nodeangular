const express = require('express');

const bodyParser = require('body-parser');

require('dotenv').config();

/**
 * appel routes routes/index
 */
const routes = require('./routes');

const dbUser = require('./controllers/randomuser.me');
/**
 * import donne randomuser.me si vide
 */
dbUser.ImportUsers();

const app = express();

app.use(bodyParser.json());
/**
 * gestion Access
 */
app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

app.use('/', routes);

const port = process.env.PORT || 8080;

app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});


