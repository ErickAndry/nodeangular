import { Component, OnInit } from '@angular/core';
import { MdbModalRef, MdbModalService } from 'mdb-angular-ui-kit/modal';
import { UserService } from 'src/app/service/user.service';
import { ModalComponent } from '../../modal/modal.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  modalRef: MdbModalRef<ModalComponent> | null = null;

  constructor(
    private userService: UserService,
    private modalService: MdbModalService
  ) {}

  listeUsers: any[] = [];
  details: any;

  pageOfItems: Array<any>;

  ngOnInit(): void {
    this.getAllListUser();
  }

  /**
   * get list utilisateur
   */
  getAllListUser() {
    this.userService.getListUser().subscribe((res) => {
      if (res.status === 200) {
        this.listeUsers = res.body.users;
      }
    });
  }

  /**
   * get details utilisateur par id
   * @param id
   */
  getDetailById(id: String) {
    this.userService.getDetailsById(id).subscribe((res) => {
      if (res.status === 200) {
        this.details = res.body.user;
        this.openModal(this.details);
      }
    });
  }

  /**
   * afficher details utilisateur
   * @param details
   */
  openModal(details) {
    this.modalRef = this.modalService.open(ModalComponent, {
      data: { details: details },
    });
  }

  /**
   * pagination liste
   * @param pageOfItems
   */
  onChangePage(pageOfItems: Array<any>) {
    // update current page of items
    this.pageOfItems = pageOfItems;
  }
}
