import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}

  /**
   *
   * get liste utilisateur
   */
  getListUser() {
    const headers = new HttpHeaders().set('content-type', 'application/json');
    return this.http.get<any>(`${environment.apiUrl}users`, {
      headers: headers,
      observe: 'response',
    });
  }

  /**
   * get details utilisateur par is
   * @param id
   */
  getDetailsById(id: String) {
    const headers = new HttpHeaders().set('content-type', 'application/json');
    return this.http.get<any>(`${environment.apiUrl}users/${id}`, {
      headers: headers,
      observe: 'response',
    });
  }
}
